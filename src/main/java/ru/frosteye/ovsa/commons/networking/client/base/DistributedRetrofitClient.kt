package ru.frosteye.ovsa.commons.networking.client.base

import ru.frosteye.ovsa.commons.networking.client.support.RestApi
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory

abstract class DistributedRetrofitClient(
    private val apis: Set<Class<*>>
) : ApiClient {

    private var map: MutableMap<Class<*>, Any> = mutableMapOf()

    override fun <T : @RestApi Any> apiFor(clazz: Class<T>): T {
        val result: Any = map[clazz]
            ?: throw IllegalArgumentException("no $clazz in $this")
        if (clazz.isInstance(result)) {
            return result as T
        } else {
            throw IllegalArgumentException("$result is not $clazz")
        }
    }

    protected open fun <T> createApi(apiClass: Class<T>): T {
        if (apiClass.getAnnotation(RestApi::class.java) == null) {
            throw IllegalArgumentException("$apiClass must be annotated as ${RestApi::class.java}")
        }

        return buildOrGetRetrofit(apiClass).create(apiClass)
    }

    protected fun createDefaultCallAdapters(): MutableList<CallAdapter.Factory> {
        return mutableListOf<CallAdapter.Factory>().apply {
            add(RxJava3CallAdapterFactory.create())
        }
    }

    @Synchronized
    protected fun createApis() {
        map = apis.associateWith { createApi(it) }.toMutableMap()
    }

    override fun recreateApis() {
        createApis()
    }

    protected abstract fun buildOrGetRetrofit(apiClass: Class<*>): Retrofit
}