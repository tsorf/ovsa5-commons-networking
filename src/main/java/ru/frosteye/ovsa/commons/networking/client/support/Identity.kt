package ru.frosteye.ovsa.commons.networking.client.support

interface Identity {

    fun identityData(): Map<String, String>
}

interface IdentityProvider {

    fun provideIdentity(): Identity?
}

class SimpleTokenIdentityProvider(
    private val identity: TokenIdentity
) : IdentityProvider {
    override fun provideIdentity(): Identity? = identity

}

class TokenIdentity(
    private val token: String?
) : Identity {

    override fun identityData(): Map<String, String> = mutableMapOf<String, String>().apply {
        put("Content-Type", "application/json")
        token?.let {
            put("token", "$it")
        }
    }

}