package ru.frosteye.ovsa.commons.networking.client.impl

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import ru.frosteye.ovsa.commons.networking.client.base.DistributedRetrofitClient
import ru.frosteye.ovsa.commons.networking.client.base.RetrofitCache
import ru.frosteye.ovsa.commons.networking.client.support.EndpointProvider
import ru.frosteye.ovsa.commons.networking.client.support.IdentityProvider
import ru.frosteye.ovsa.commons.networking.client.support.TransientExclusionStrategy
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class RestClient @Inject constructor(
    private val identityProvider: IdentityProvider,
    private val endpointProvider: EndpointProvider,
    private val retrofitCache: RetrofitCache,
    apis: Set<Class<*>>
) : DistributedRetrofitClient(apis) {

    init {
        createApis()
    }

    private var sharedOkHttpClient: OkHttpClient? = null


    private fun url(apiClass: Class<*>): String {
        return endpointProvider.endpointForApi(apiClass)
    }


    override fun buildOrGetRetrofit(apiClass: Class<*>): Retrofit {
        val url = url(apiClass)
        return retrofitCache.get(url) ?: run {
            val newInstance = createRetrofit(apiClass)
            retrofitCache.put(url, newInstance)
            return newInstance
        }
    }

    private fun identityProvider(apiClass: Class<*>): IdentityProvider? {
        return identityProvider
    }

    private fun createOkHttpClient(apiClass: Class<*>): OkHttpClient {
        var client = sharedOkHttpClient
        return if (client == null) {
            client = OkHttpClient.Builder()
                .apply {
                    connectTimeout(connectionTimeout, TimeUnit.SECONDS)
                    readTimeout(readTimeout, TimeUnit.SECONDS)
                    writeTimeout(writeTimeout, TimeUnit.SECONDS)
                    retryOnConnectionFailure(true)
                    createClientInterceptors().forEach {
                        addInterceptor(it)
                    }
                    addInterceptor(
                        HttpLoggingInterceptor {
                            Log.d("RestClient", it)
                        }.setLevel(HttpLoggingInterceptor.Level.HEADERS)
                    )
                }
                .build()
            client
        } else {
            client
        }
    }

    protected open fun createClientInterceptors(): MutableList<Interceptor> {
        return mutableListOf<Interceptor>().apply {
            add(Interceptor { chain ->
                val builder = chain.request().newBuilder()
                identityProvider.provideIdentity()?.identityData()?.let {
                    for ((key, value) in it) {
                        builder.addHeader(key, value)
                    }
                }
                chain.proceed(builder.build())
            })
        }
    }

    private fun createRetrofit(apiClass: Class<*>): Retrofit {

        val gsonBuilder = GsonBuilder()
            .setExclusionStrategies(TransientExclusionStrategy)
            .setLenient()

        val retrofitBuilder = Retrofit.Builder().apply {
            baseUrl(url(apiClass))
            client(createOkHttpClient(apiClass))
            addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
            addConverterFactory(ScalarsConverterFactory.create())
            val callAdapters = mutableListOf(
                RxJava3CallAdapterFactory.create()
            )
            callAdapters.forEach {
                addCallAdapterFactory(it)
            }
        }

        return retrofitBuilder.build()
    }

    companion object Defaults {

        const val connectionTimeout = 15L
        const val writeTimeout = 60L
        const val readTimeout = 60L
    }
}