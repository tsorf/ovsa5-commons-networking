package ru.frosteye.ovsa.commons.networking.client.base

interface ApiClient {

    /**
     * Получает (или создает в случае отсутствия)
     * кешированный экземпляр api-сервиса
     * @param clazz класс сервиса (должен быть аннотирован как [RestApi]
     */
    fun <T : Any> apiFor(clazz: Class<T>): T

    /**
     * Пересоздает все api на основе изменившихся учетных данных
     */
    fun recreateApis()
}