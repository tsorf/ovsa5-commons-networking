package ru.frosteye.ovsa.commons.networking.client.support

import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes

object TransientExclusionStrategy : ExclusionStrategy {
    override fun shouldSkipClass(type: Class<*>): Boolean = false
    override fun shouldSkipField(f: FieldAttributes): Boolean =
        f.getAnnotation(GsonTransient::class.java) != null
                || f.name.endsWith("\$delegate")
}