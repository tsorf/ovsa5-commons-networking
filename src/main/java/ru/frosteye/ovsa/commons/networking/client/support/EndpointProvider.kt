package ru.frosteye.ovsa.commons.networking.client.support

interface EndpointProvider {

    fun endpointForApi(apiClass: Class<*>): String
}