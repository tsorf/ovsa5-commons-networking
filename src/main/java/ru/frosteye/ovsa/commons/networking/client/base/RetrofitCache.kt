package ru.frosteye.ovsa.commons.networking.client.base

import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RetrofitCache @Inject constructor() {

    private val map = mutableMapOf<String, Retrofit>()

    fun get(url: String): Retrofit? {
        synchronized(map) {
            return map[url]
        }
    }

    fun put(url: String, retrofit: Retrofit) {
        synchronized(map) {
            map[url] = retrofit
        }
    }
}